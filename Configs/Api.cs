﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Configs
{
    internal static class Api
    {   // Link da nossa api
        public static string ApiUrl { get => "http://10.67.44.31:5000/api"; }
        //public static string ApiUrl { get => "http://708c-177-47-86-186.ngrok.io/api"; }
        private static string Token;
        private static int UserID = -1;
        public static RestClient Client(string path)
        {
            var client = new RestClient($"{ApiUrl}{path}");
            if (Token != null)
                client.Authenticator = new JwtAuthenticator(Token);
            return client;
        }
        public static void SaveToken(string token, int id)
        {
            Token = token;
            UserID = id;
        }
        public static int GetUserID => UserID;
    }
}
