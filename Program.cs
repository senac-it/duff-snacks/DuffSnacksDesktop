﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DuffSnacksDesktop.Views.Categorias;
using DuffSnacksDesktop.Views.Pacotes;
using DuffSnacksDesktop.Views.Principal;
using DuffSnacksDesktop.Views.Produtos;
using DuffSnacksDesktop.Views.TelaInicial;
using DuffSnacksDesktop.Views.Usuarios;

namespace DuffSnacksDesktop
{
    internal static class Program
    {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Principal());
        }
    }
}
