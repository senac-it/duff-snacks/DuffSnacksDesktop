﻿namespace DuffSnacksDesktop.Views.Vendas
{
    partial class VendaGerenciamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectProduct = new System.Windows.Forms.ComboBox();
            this.InputID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ButtonRemoverProduct = new System.Windows.Forms.Button();
            this.ButtonAddProduct = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.ListaProdutos = new System.Windows.Forms.ListBox();
            this.ButtonClose = new System.Windows.Forms.Button();
            this.ButtonDeletar = new System.Windows.Forms.Button();
            this.ButtonSave = new System.Windows.Forms.Button();
            this.InputPrice = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.SelectPacotes = new System.Windows.Forms.ComboBox();
            this.RemovePacote = new System.Windows.Forms.Button();
            this.AddPacote = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ListaPacotes = new System.Windows.Forms.ListBox();
            this.SelectClient = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SelectProduct
            // 
            this.SelectProduct.FormattingEnabled = true;
            this.SelectProduct.Items.AddRange(new object[] {
            "Valor1",
            "Valor2",
            "Valor3",
            "Valor4",
            "Valor5"});
            this.SelectProduct.Location = new System.Drawing.Point(82, 197);
            this.SelectProduct.Name = "SelectProduct";
            this.SelectProduct.Size = new System.Drawing.Size(175, 21);
            this.SelectProduct.TabIndex = 17;
            // 
            // InputID
            // 
            this.InputID.Enabled = false;
            this.InputID.Location = new System.Drawing.Point(0, 16);
            this.InputID.Name = "InputID";
            this.InputID.Size = new System.Drawing.Size(65, 20);
            this.InputID.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "ID";
            // 
            // ButtonRemoverProduct
            // 
            this.ButtonRemoverProduct.Enabled = false;
            this.ButtonRemoverProduct.Location = new System.Drawing.Point(0, 196);
            this.ButtonRemoverProduct.Name = "ButtonRemoverProduct";
            this.ButtonRemoverProduct.Size = new System.Drawing.Size(75, 23);
            this.ButtonRemoverProduct.TabIndex = 14;
            this.ButtonRemoverProduct.Text = "Remover";
            this.ButtonRemoverProduct.UseVisualStyleBackColor = true;
            this.ButtonRemoverProduct.Click += new System.EventHandler(this.ButtonRemoverProduct_Click);
            // 
            // ButtonAddProduct
            // 
            this.ButtonAddProduct.Location = new System.Drawing.Point(263, 196);
            this.ButtonAddProduct.Name = "ButtonAddProduct";
            this.ButtonAddProduct.Size = new System.Drawing.Size(75, 23);
            this.ButtonAddProduct.TabIndex = 13;
            this.ButtonAddProduct.Text = "Adicionar";
            this.ButtonAddProduct.UseVisualStyleBackColor = true;
            this.ButtonAddProduct.Click += new System.EventHandler(this.ButtonAddProduct_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Produtos";
            // 
            // ListaProdutos
            // 
            this.ListaProdutos.FormattingEnabled = true;
            this.ListaProdutos.Location = new System.Drawing.Point(0, 95);
            this.ListaProdutos.Name = "ListaProdutos";
            this.ListaProdutos.Size = new System.Drawing.Size(338, 95);
            this.ListaProdutos.TabIndex = 11;
            this.ListaProdutos.SelectedIndexChanged += new System.EventHandler(this.ListaProdutos_SelectedIndexChanged);
            // 
            // ButtonClose
            // 
            this.ButtonClose.Location = new System.Drawing.Point(607, 240);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(75, 23);
            this.ButtonClose.TabIndex = 8;
            this.ButtonClose.Text = "Fechar";
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.ButtonClose_Click);
            // 
            // ButtonDeletar
            // 
            this.ButtonDeletar.Enabled = false;
            this.ButtonDeletar.Location = new System.Drawing.Point(81, 240);
            this.ButtonDeletar.Name = "ButtonDeletar";
            this.ButtonDeletar.Size = new System.Drawing.Size(75, 23);
            this.ButtonDeletar.TabIndex = 7;
            this.ButtonDeletar.Text = "Deletar";
            this.ButtonDeletar.UseVisualStyleBackColor = true;
            this.ButtonDeletar.Click += new System.EventHandler(this.ButtonDeletar_Click);
            // 
            // ButtonSave
            // 
            this.ButtonSave.Location = new System.Drawing.Point(0, 240);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(75, 23);
            this.ButtonSave.TabIndex = 6;
            this.ButtonSave.Text = "Salvar";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // InputPrice
            // 
            this.InputPrice.Enabled = false;
            this.InputPrice.Location = new System.Drawing.Point(607, 55);
            this.InputPrice.Name = "InputPrice";
            this.InputPrice.Size = new System.Drawing.Size(75, 20);
            this.InputPrice.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(604, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Preço";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-3, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente";
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.SelectPacotes);
            this.panel1.Controls.Add(this.RemovePacote);
            this.panel1.Controls.Add(this.AddPacote);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.ListaPacotes);
            this.panel1.Controls.Add(this.SelectClient);
            this.panel1.Controls.Add(this.SelectProduct);
            this.panel1.Controls.Add(this.InputID);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.ButtonRemoverProduct);
            this.panel1.Controls.Add(this.ButtonAddProduct);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.ListaProdutos);
            this.panel1.Controls.Add(this.ButtonClose);
            this.panel1.Controls.Add(this.ButtonDeletar);
            this.panel1.Controls.Add(this.ButtonSave);
            this.panel1.Controls.Add(this.InputPrice);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(16, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(767, 351);
            this.panel1.TabIndex = 1;
            // 
            // SelectPacotes
            // 
            this.SelectPacotes.FormattingEnabled = true;
            this.SelectPacotes.Items.AddRange(new object[] {
            "Valor1",
            "Valor2",
            "Valor3",
            "Valor4",
            "Valor5"});
            this.SelectPacotes.Location = new System.Drawing.Point(426, 197);
            this.SelectPacotes.Name = "SelectPacotes";
            this.SelectPacotes.Size = new System.Drawing.Size(175, 21);
            this.SelectPacotes.TabIndex = 23;
            // 
            // RemovePacote
            // 
            this.RemovePacote.Enabled = false;
            this.RemovePacote.Location = new System.Drawing.Point(344, 196);
            this.RemovePacote.Name = "RemovePacote";
            this.RemovePacote.Size = new System.Drawing.Size(75, 23);
            this.RemovePacote.TabIndex = 22;
            this.RemovePacote.Text = "Remover";
            this.RemovePacote.UseVisualStyleBackColor = true;
            this.RemovePacote.Click += new System.EventHandler(this.RemovePacote_Click);
            // 
            // AddPacote
            // 
            this.AddPacote.Location = new System.Drawing.Point(607, 196);
            this.AddPacote.Name = "AddPacote";
            this.AddPacote.Size = new System.Drawing.Size(75, 23);
            this.AddPacote.TabIndex = 21;
            this.AddPacote.Text = "Adicionar";
            this.AddPacote.UseVisualStyleBackColor = true;
            this.AddPacote.Click += new System.EventHandler(this.AddPacote_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(344, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Pacotes";
            // 
            // ListaPacotes
            // 
            this.ListaPacotes.FormattingEnabled = true;
            this.ListaPacotes.Location = new System.Drawing.Point(344, 95);
            this.ListaPacotes.Name = "ListaPacotes";
            this.ListaPacotes.Size = new System.Drawing.Size(338, 95);
            this.ListaPacotes.TabIndex = 19;
            this.ListaPacotes.SelectedIndexChanged += new System.EventHandler(this.ListaPacotes_SelectedIndexChanged);
            // 
            // SelectClient
            // 
            this.SelectClient.FormattingEnabled = true;
            this.SelectClient.Location = new System.Drawing.Point(0, 55);
            this.SelectClient.Name = "SelectClient";
            this.SelectClient.Size = new System.Drawing.Size(601, 21);
            this.SelectClient.TabIndex = 18;
            // 
            // VendaGerenciamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(799, 383);
            this.Controls.Add(this.panel1);
            this.Name = "VendaGerenciamento";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Text = "Gerenciamento de Venda";
            this.Load += new System.EventHandler(this.VendaGerenciamento_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox SelectProduct;
        private System.Windows.Forms.TextBox InputID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ButtonRemoverProduct;
        private System.Windows.Forms.Button ButtonAddProduct;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox ListaProdutos;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.Button ButtonDeletar;
        private System.Windows.Forms.Button ButtonSave;
        private System.Windows.Forms.TextBox InputPrice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox SelectPacotes;
        private System.Windows.Forms.Button RemovePacote;
        private System.Windows.Forms.Button AddPacote;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox ListaPacotes;
        private System.Windows.Forms.ComboBox SelectClient;
    }
}