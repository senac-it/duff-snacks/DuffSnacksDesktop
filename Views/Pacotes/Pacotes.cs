﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;

namespace DuffSnacksDesktop.Views.Pacotes
{
    public partial class Pacotes : Form
    {
        private readonly PacksService _packsService;
        private ICollection<Pack> Packs = new List<Pack>();
        public Pacotes()
        {
            InitializeComponent();
            _packsService = new PacksService();
        }

        private void CategoryList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Pacotes_Load(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void Pesquisa()
        {
            // Pesquisando pelos pacotes ou retornando todos se o input for vazio
            PacksList.Items.Clear();
            Packs = _packsService.GetAll(Pesquisar.Text.Trim().Split("\n".ToCharArray()).Last());
            foreach (var pack in Packs)
            {
                var item = new ListViewItem { Text = pack.Name };
                item.SubItems.Add(pack.Description);
                item.SubItems.Add(pack.Price.ToString());
                if (pack.PacksAssocs != null)
                    item.SubItems.Add(pack.PacksAssocs
                        .Select(x => x != null ? x.Product.Name : "")
                        .Aggregate((a, b) => $"{a}, {b}"));
                PacksList.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void CategoryList_DoubleClick(object sender, EventArgs e)
        {
            // Quando der click duplo abrira a tela de Gerenciamento de categoria
            if (PacksList.SelectedItems.Count == 0) return;
            var id = Packs.ElementAt(PacksList.SelectedItems[0].Index).ID;
            var form = new PacoteGerenciamento(id);
            form.ShowDialog();
            Pesquisa();
        }
    }
}
