﻿namespace DuffSnacksDesktop.Views.Categorias
{
    partial class CategoriaGerenciamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.InputID = new System.Windows.Forms.TextBox();
            this.ButtonSave = new System.Windows.Forms.Button();
            this.ButtonClose = new System.Windows.Forms.Button();
            this.ButtonDelete = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.InputName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.InputDescription = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.InputID);
            this.panel1.Controls.Add(this.ButtonSave);
            this.panel1.Controls.Add(this.ButtonClose);
            this.panel1.Controls.Add(this.ButtonDelete);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.InputName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.InputDescription);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(16, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(624, 359);
            this.panel1.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "ID";
            // 
            // InputID
            // 
            this.InputID.Enabled = false;
            this.InputID.Location = new System.Drawing.Point(0, 16);
            this.InputID.Name = "InputID";
            this.InputID.Size = new System.Drawing.Size(52, 20);
            this.InputID.TabIndex = 27;
            // 
            // ButtonSave
            // 
            this.ButtonSave.Location = new System.Drawing.Point(0, 235);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(133, 23);
            this.ButtonSave.TabIndex = 20;
            this.ButtonSave.Text = "Salvar";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // ButtonClose
            // 
            this.ButtonClose.Location = new System.Drawing.Point(320, 235);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(133, 23);
            this.ButtonClose.TabIndex = 26;
            this.ButtonClose.Text = "Fechar";
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.button3_Click);
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Enabled = false;
            this.ButtonDelete.Location = new System.Drawing.Point(160, 235);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(133, 23);
            this.ButtonDelete.TabIndex = 21;
            this.ButtonDelete.Text = "Deletar";
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-3, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Descrição";
            // 
            // InputName
            // 
            this.InputName.Location = new System.Drawing.Point(0, 55);
            this.InputName.Name = "InputName";
            this.InputName.Size = new System.Drawing.Size(453, 20);
            this.InputName.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-3, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Nome";
            // 
            // InputDescription
            // 
            this.InputDescription.Location = new System.Drawing.Point(0, 94);
            this.InputDescription.Multiline = true;
            this.InputDescription.Name = "InputDescription";
            this.InputDescription.Size = new System.Drawing.Size(453, 135);
            this.InputDescription.TabIndex = 23;
            // 
            // CategoriaGerenciamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(656, 391);
            this.Controls.Add(this.panel1);
            this.Name = "CategoriaGerenciamento";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Text = "Gerenciamento de Categoria";
            this.Load += new System.EventHandler(this.CategoriaCadastro_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox InputID;
        private System.Windows.Forms.Button ButtonSave;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.Button ButtonDelete;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox InputName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox InputDescription;
    }
}