﻿using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using DuffSnacksDesktop.Views.Clientes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Views.Usuarios
{
    public partial class Usuarios : Form
    {
        private readonly UsersService _usersService;
        private ICollection<User> Users = new List<User>();
        public Usuarios()
        {
            InitializeComponent();
            _usersService= new UsersService();
        }

        private void Usuarios_Load(object sender, EventArgs e)
        {
            Pesquisa();
        }
        private void Pesquisa()
        {
            // Fazendo pesquisa dos usuarios ou retornando todos se o input for vazio
            UsersList.Items.Clear();
            Users = _usersService.GetAll(Pesquisar.Text.Trim().Split("\n".ToCharArray()).Last());
            if (Users == null ) { Close(); return; }
            foreach (var user in Users)
            {
                var item = new ListViewItem { Text = user.Username };
                item.SubItems.Add(user.Role.Name);
                UsersList.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void UsersList_DoubleClick(object sender, EventArgs e)
        {
            // Quando der o click duplo vai abrir a tela de Gerenciamento de usuario
            if (UsersList.SelectedItems.Count == 0) return;
            var id = Users.ElementAt(UsersList.SelectedItems[0].Index).ID;
            var form = new UsuarioGerenciamento(id);
            form.ShowDialog();
            Pesquisa();
        }
    }
}
