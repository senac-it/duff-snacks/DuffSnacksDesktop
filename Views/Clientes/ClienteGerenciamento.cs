﻿using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Views.Clientes
{
    public partial class ClienteGerenciamento : Form
    {
        private readonly ClientsService  _clientsService = new ClientsService();
        private int ID = -1;
        public ClienteGerenciamento()
        {
            InitializeComponent();
        }
        public ClienteGerenciamento(int id)
        {
            InitializeComponent();
            ID = id;
        }

        private void ClienteGerenciamento_Load(object sender, EventArgs e)
        {
            // se o ID não for igual a -1 vai passar para atualizar o cliente
            if (ID == -1) return;

            // Pegando um cliente para atualizar
            var client = _clientsService.GetOne(ID);
            // Checando se o cliente existe se nao ele retorna e não faz nada
            if (client == null) return;

            // Passando os valores para os inputs e habilitando os botões necessarios
            ButtonDelete.Enabled = true;
            InputID.Text = client.ID.ToString();
            InputName.Text = client.Fullname;
            InputEmail.Text = client.Email;
            InputDocumento.Text = client.Document;
            InputEndereco.Text = client.Address;
        }

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (ID == -1)
            {
                // Criando um cliente se o id for -1
                if (_clientsService.CreateOne(new Client
                {
                    Fullname = InputName.Text,
                    Email = InputEmail.Text,
                    Document = InputDocumento.Text,
                    Address = InputEndereco.Text,
                })) Close();
            }
            else
            {
                // Atualizando um cliente se o id não for -1
                if (_clientsService.UpdateOne(ID, new Client
                {
                    Fullname = InputName.Text,
                    Email = InputEmail.Text,
                    Document = InputDocumento.Text,
                    Address = InputEndereco.Text,
                })) Close();
            }
        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            // Deletando o cliente e fechando a tela
            if (_clientsService.DeleteOne(ID)) Close();
        }
    }
}
