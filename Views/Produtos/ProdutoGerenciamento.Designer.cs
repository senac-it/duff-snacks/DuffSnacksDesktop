﻿namespace DuffSnacksDesktop.Views.Produtos
{
    partial class ProdutoGerenciamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.InputPorcentagem = new System.Windows.Forms.CheckBox();
            this.SelectCategory = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.InputPrice = new System.Windows.Forms.TextBox();
            this.InputDiscount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.InputID = new System.Windows.Forms.TextBox();
            this.ButtonClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.InputDescription = new System.Windows.Forms.TextBox();
            this.InputNome = new System.Windows.Forms.TextBox();
            this.ButtonDelete = new System.Windows.Forms.Button();
            this.ButtonSave = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add(this.InputPorcentagem);
            this.panel1.Controls.Add(this.SelectCategory);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.InputPrice);
            this.panel1.Controls.Add(this.InputDiscount);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.InputID);
            this.panel1.Controls.Add(this.ButtonClose);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.InputDescription);
            this.panel1.Controls.Add(this.InputNome);
            this.panel1.Controls.Add(this.ButtonDelete);
            this.panel1.Controls.Add(this.ButtonSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(16, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1001, 380);
            this.panel1.TabIndex = 0;
            // 
            // InputPorcentagem
            // 
            this.InputPorcentagem.AutoSize = true;
            this.InputPorcentagem.Location = new System.Drawing.Point(586, 96);
            this.InputPorcentagem.Name = "InputPorcentagem";
            this.InputPorcentagem.Size = new System.Drawing.Size(155, 17);
            this.InputPorcentagem.TabIndex = 44;
            this.InputPorcentagem.Text = "Desconto em Porcentagem";
            this.InputPorcentagem.UseVisualStyleBackColor = true;
            // 
            // SelectCategory
            // 
            this.SelectCategory.FormattingEnabled = true;
            this.SelectCategory.Items.AddRange(new object[] {
            "Salgadinho",
            "Energeticos",
            "Refrigerantes",
            "Acompanhamento"});
            this.SelectCategory.Location = new System.Drawing.Point(586, 54);
            this.SelectCategory.Name = "SelectCategory";
            this.SelectCategory.Size = new System.Drawing.Size(190, 21);
            this.SelectCategory.TabIndex = 43;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(583, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 42;
            this.label6.Text = "Categoria";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(456, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Desconto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(456, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Preço";
            // 
            // InputPrice
            // 
            this.InputPrice.Location = new System.Drawing.Point(459, 55);
            this.InputPrice.Name = "InputPrice";
            this.InputPrice.Size = new System.Drawing.Size(121, 20);
            this.InputPrice.TabIndex = 39;
            // 
            // InputDiscount
            // 
            this.InputDiscount.Location = new System.Drawing.Point(459, 94);
            this.InputDiscount.Name = "InputDiscount";
            this.InputDiscount.Size = new System.Drawing.Size(121, 20);
            this.InputDiscount.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "ID";
            // 
            // InputID
            // 
            this.InputID.Enabled = false;
            this.InputID.Location = new System.Drawing.Point(0, 16);
            this.InputID.Name = "InputID";
            this.InputID.Size = new System.Drawing.Size(52, 20);
            this.InputID.TabIndex = 36;
            // 
            // ButtonClose
            // 
            this.ButtonClose.Location = new System.Drawing.Point(679, 206);
            this.ButtonClose.Name = "ButtonClose";
            this.ButtonClose.Size = new System.Drawing.Size(97, 23);
            this.ButtonClose.TabIndex = 35;
            this.ButtonClose.Text = "Fechar";
            this.ButtonClose.UseVisualStyleBackColor = true;
            this.ButtonClose.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(-3, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 34;
            this.label2.Text = "Descrição";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(-3, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Nome";
            // 
            // InputDescription
            // 
            this.InputDescription.Location = new System.Drawing.Point(0, 94);
            this.InputDescription.Multiline = true;
            this.InputDescription.Name = "InputDescription";
            this.InputDescription.Size = new System.Drawing.Size(453, 135);
            this.InputDescription.TabIndex = 32;
            // 
            // InputNome
            // 
            this.InputNome.Location = new System.Drawing.Point(0, 55);
            this.InputNome.Name = "InputNome";
            this.InputNome.Size = new System.Drawing.Size(453, 20);
            this.InputNome.TabIndex = 31;
            // 
            // ButtonDelete
            // 
            this.ButtonDelete.Enabled = false;
            this.ButtonDelete.Location = new System.Drawing.Point(569, 206);
            this.ButtonDelete.Name = "ButtonDelete";
            this.ButtonDelete.Size = new System.Drawing.Size(97, 23);
            this.ButtonDelete.TabIndex = 30;
            this.ButtonDelete.Text = "Deletar";
            this.ButtonDelete.UseVisualStyleBackColor = true;
            this.ButtonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // ButtonSave
            // 
            this.ButtonSave.Location = new System.Drawing.Point(459, 206);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(97, 23);
            this.ButtonSave.TabIndex = 29;
            this.ButtonSave.Text = "Salvar";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // ProdutoGerenciamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1033, 412);
            this.Controls.Add(this.panel1);
            this.Name = "ProdutoGerenciamento";
            this.Padding = new System.Windows.Forms.Padding(16);
            this.Text = "Gerenciamento de Produto";
            this.Load += new System.EventHandler(this.ProdutoGerenciamento_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox InputPorcentagem;
        private System.Windows.Forms.ComboBox SelectCategory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox InputPrice;
        private System.Windows.Forms.TextBox InputDiscount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox InputID;
        private System.Windows.Forms.Button ButtonClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox InputDescription;
        private System.Windows.Forms.TextBox InputNome;
        private System.Windows.Forms.Button ButtonDelete;
        private System.Windows.Forms.Button ButtonSave;
    }
}