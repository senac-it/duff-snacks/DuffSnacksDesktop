﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Models.DTOs;
using DuffSnacksDesktop.Repositories;
using Newtonsoft.Json;
using RestSharp;

namespace DuffSnacksDesktop.Services
{
    internal class CategoriesService
    {
        public ICollection<Category> GetAll(string keyword = "")
        {
            var client = Api.Client($"/Categories");
            var request = new RestRequest();
            request.AddParameter("keyword", keyword);
            var response = client.Get<ICollection<Category>>(request);
            return response;
        }

        public Category GetOne(int id)
        {
            var client = Api.Client($"/Categories/{id}");
            var request = new RestRequest();
            var response = client.ExecuteGet<Category>(request);

            if (!response.IsSuccessStatusCode)
            {

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());

                return null;
            }

            return response.Data;
        }

        public bool CreateOne(string name, string description)
        {
            try
            {
                var client = Api.Client("/Categories");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Name = name,
                    Description = description,
                });
                var response = client.ExecutePost<Category>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Categoria criada com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel criar a categoria tente novamente");
                return false;
            }
        }

        public bool UpdateOne(int id, string name, string description)
        {
            try
            {
                var client = Api.Client($"/Categories/{id}");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Name = name,
                    Description = description,
                });
                var response = client.ExecutePut<Category>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Categoria atualizada com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar a categoria tente novamente");
                return false;
            }
        }

        public bool DeleteOne(int id)
        {
            try
            {
                var client = Api.Client($"/Categories/{id}");
                var request = new RestRequest();
                var response = client.Delete<Category>(request);
                MessageBox.Show("Categoria deletada");
                return true;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar a categoria tente novamente");
                return false;
            }
        }
    }
}
