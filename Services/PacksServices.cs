﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Models.DTOs;
using DuffSnacksDesktop.Repositories;
using Newtonsoft.Json;
using RestSharp;

namespace DuffSnacksDesktop.Services
{
    internal class PacksService
    {
        public ICollection<Pack> GetAll(string keyword = "")
        {
            var client = Api.Client("/Packs");
            var request = new RestRequest();
            request.AddParameter("keyword", keyword);
            var response = client.Get<ICollection<Pack>>(request);
            return response;
        }

        public Pack GetOne(int id)
        {
            var client = Api.Client($"/Packs/{id}");
            var request = new RestRequest();
            var response = client.ExecuteGet<Pack>(request);

            if (!response.IsSuccessStatusCode)
            {

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());

                return null;
            }

            return response.Data;
        }

        public bool CreateOne(Pack data)
        {
            try
            {
                var client = Api.Client("/Packs");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Name = data.Name,
                    Description = data.Description,
                    Disabled = data.Disabled,
                    Price = data.Price,
                    PacksAssocs = data.PacksAssocs,
                }); 
                var response = client.ExecutePost<Pack>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Pacote criado com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel criar o Pacote tente novamente");
                return false;
            }
        }

        public bool UpdateOne(int id, Pack data)
        {
            try
            {
                var client = Api.Client($"/Packs/{id}");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Name = data.Name,
                    Description = data.Description,
                    Disabled = data.Disabled,
                    Price = data.Price,
                    PacksAssocs = data.PacksAssocs,
                });
                var response = client.ExecutePut<Pack>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Pacote atualizado com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar o Pacote tente novamente");
                return false;
            }
        }

        public bool DeleteOne(int id)
        {
            try
            {
                var client = Api.Client($"/Packs/{id}");
                var request = new RestRequest();
                var response = client.Delete<Pack>(request);
                MessageBox.Show("Pacote deletado");
                return true;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel deletar o Pacote tente novamente");
                return false;
            }
        }
    }
}
