﻿using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Models.DTOs;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Services
{
    internal class ClientsService
    {
        public ICollection<Client> GetAll(string keyword = "")
        {
            var client = Api.Client($"/Clients");
            var request = new RestRequest();
            request.AddParameter("keyword", keyword);
            var response = client.Get<ICollection<Client>>(request);
            return response;
        }

        public Client GetOne(int id)
        {
            var client = Api.Client($"/Clients/{id}");
            var request = new RestRequest();
            var response = client.ExecuteGet<Client>(request);

            if (!response.IsSuccessStatusCode)
            {

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());

                return null;
            }

            return response.Data;
        }

        public bool CreateOne(Client data)
        {
            try
            {
                var client = Api.Client("/Clients");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Fullname = data.Fullname,
                    Email = data.Email,
                    Document = data.Document,
                    Address = data.Address,
                }) ;
                var response = client.ExecutePost<Client>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Cliente criado com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel criar o Cliente tente novamente");
                return false;
            }
        }

        public bool UpdateOne(int id, Client data)
        {
            try
            {
                var client = Api.Client($"/Clients/{id}");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Fullname = data.Fullname,
                    Email = data.Email,
                    Document = data.Document,
                    Address = data.Address,
                });
                var response = client.ExecutePut<Client>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Cliente atualizado com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar o Cliente tente novamente");
                return false;
            }
        }

        public bool DeleteOne(int id)
        {
            try
            {
                var client = Api.Client($"/Clients/{id}");
                var request = new RestRequest();
                var response = client.Delete<Client>(request);
                MessageBox.Show("Cliente deletado");
                return true;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar o Cliente tente novamente");
                return false;
            }
        }
    }
}
