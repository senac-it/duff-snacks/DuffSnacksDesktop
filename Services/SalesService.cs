﻿using DuffSnacksDesktop.Configs;
using DuffSnacksDesktop.Models;
using DuffSnacksDesktop.Models.DTOs;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DuffSnacksDesktop.Services
{
    internal class SalesService
    {
        public ICollection<Sale> GetAll(string keyword = "")
        {
            var client = Api.Client($"/Sales");
            var request = new RestRequest();
            request.AddParameter("keyword", keyword);
            var response = client.Get<ICollection<Sale>>(request);
            return response;
        }

        public Sale GetOne(int id)
        {
            var client = Api.Client($"/Sales/{id}");
            var request = new RestRequest();
            var response = client.ExecuteGet<Sale>(request);

            if (!response.IsSuccessStatusCode)
            {
                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());

                return null;
            }

            return response.Data;
        }

        public bool CreateOne(
            double price,
            int clientID,
            ICollection<int> packsIDs,
            ICollection<int> productsIDs
        )
        {
            try
            {
                var client = Api.Client("/Sales");
                var request = new RestRequest();
                request.AddBody(new
                {
                    UserID = Api.GetUserID,
                    Price = price,
                    ClientID = clientID,
                    BuyPacksIDs = packsIDs,
                    BuyProductsIDs = productsIDs,
                });
                var response = client.ExecutePost<Pack>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Venda criada com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel criar a Venda tente novamente");
                return false;
            }
        }

        public bool UpdateOne(
            int id,
            double price,
            ICollection<int> packsIDs,
            ICollection<int> productsIDs
        )
        {
            try
            {
                var client = Api.Client($"/Sales/{id}");
                var request = new RestRequest();
                request.AddBody(new
                {
                    Price = price,
                    BuyPacksIDs = packsIDs,
                    BuyProductsIDs = productsIDs,
                });
                var response = client.ExecutePut<Sale>(request);
                if (response.IsSuccessful)
                {
                    MessageBox.Show("Venda atualizada com sucesso!");
                    return true;
                }

                ErrorDTO errorContent = JsonConvert.DeserializeObject<ErrorDTO>(response.Content);
                MessageBox.Show(errorContent.Errors.Values.First().First().ToString());
                return false;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel atualizar a Venda tente novamente");
                return false;
            }
        }

        public bool DeleteOne(int id)
        {
            try
            {
                var client = Api.Client($"/Sales/{id}");
                var request = new RestRequest();
                var response = client.Delete<Sale>(request);
                MessageBox.Show("Venda deletada");
                return true;
            }
            catch
            {
                MessageBox.Show("Nâo foi possivel deletar a Venda tente novamente");
                return false;
            }
        }
    }
}
