﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models.DTOs
{
    internal class AuthLoginResponseDTO
    {
        public string Token { get; set; }
        public int ID { get; set; }
    }
}
