﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models.DTOs
{
    internal class ErrorDTO
    {
        public IDictionary<string, ICollection<string>> Errors { get; set; }
    }
}
