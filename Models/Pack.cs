﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models
{
    internal class Pack : BaseModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool Disabled { get; set; }

        public double Price { get; set; }

        public ICollection<PackAssoc> PacksAssocs { get; set; }

    }
}
