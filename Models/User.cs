﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models
{
    internal class User : BaseModel 
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public int RoleID { get; set; }
        public Role Role { get; set; }

    }
}
