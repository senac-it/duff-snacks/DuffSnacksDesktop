﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models
{
    internal class BuyPack : BaseModel
    {
        public int PackID { get; set; }
        public Pack Pack { get; set; }

        public int SaleID { get; set; }
        public Sale Sale { get; set; }
    }
}
