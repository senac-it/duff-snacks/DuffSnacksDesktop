﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuffSnacksDesktop.Models
{
    internal class Role : BaseModel
    {
        public string Name { get; set; }
        public bool IsPrivileged { get; set; }
    }
}
