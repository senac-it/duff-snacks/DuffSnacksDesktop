﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuffSnacksDesktop.Models;

namespace DuffSnacksDesktop.Repositories
{
    internal interface IPacksAssocsRepository
    {
        ICollection<PackAssoc> GetAll();
        PackAssoc GetById(int id);
        void Create(PackAssoc packsAssoc);
        void Update(PackAssoc packsAssoc);
        void Delete(int id);
    }
}
