﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuffSnacksDesktop.Models;

namespace DuffSnacksDesktop.Repositories
{
    internal class PacksRepository : IPacksRepository
    {
        private static IPacksRepository Self { get; set; }
        private IPacksAssocsRepository _packsAssocsRepository { get; set; }

        private ICollection<Pack> Packs = new List<Pack>
        {
            new Pack { ID = 1, Name = "Promoção 1", Price = 0.10, Description = "Description" },
            new Pack { ID = 2, Name = "Promoção 2", Price = 0.10, Description = "Description" },
            new Pack { ID = 3, Name = "Promoção 3", Price = 0.10, Description = "Description" },
            new Pack { ID = 4, Name = "Promoção 4", Price = 0.10, Description = "Description" },
        };

        public static IPacksRepository New()
        {
            if (Self == null) Self = new PacksRepository();
            return Self;
        }
        private PacksRepository()
        {
            _packsAssocsRepository = PacksAssocsRepository.New();
        }
        public void Create(Pack pack)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<Pack> GetAll()
        {
            return Packs;
        }

        public Pack GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(Pack pack)
        {
            throw new NotImplementedException();
        }
    }
}
