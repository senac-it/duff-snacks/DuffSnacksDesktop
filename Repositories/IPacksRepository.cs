﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuffSnacksDesktop.Models;

namespace DuffSnacksDesktop.Repositories
{
    internal interface IPacksRepository
    {
        ICollection<Pack> GetAll();
        Pack GetById(int id);
        void Create(Pack pack);
        void Update(Pack pack);
        void Delete(int id);
    }
}
