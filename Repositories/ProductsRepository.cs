﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuffSnacksDesktop.Models;

namespace DuffSnacksDesktop.Repositories
{
    internal class ProductsRepository : IProductsRepository
    {
        private static IProductsRepository Self { get; set; }
        private ICollection<Product> Products = new List<Product>
        {
            new Product { ID = 1, Name = "doritos", Price = 5.0, CategoryID = 1 },
            new Product { ID = 2, Name = "cebolitos", Price = 5.0, CategoryID = 1 },
            new Product { ID = 3, Name = "ruffles", Price = 5.0, CategoryID = 1 },
            new Product { ID = 4, Name = "baconzitos", Price = 5.0, CategoryID = 1 },
            new Product { ID = 5, Name = "monster", Price = 5.0, CategoryID = 3 },
            new Product { ID = 6, Name = "coca cola", Price = 5.0, CategoryID = 2 },
            new Product { ID = 7, Name = "bala fini", Price = 5.0, CategoryID = 4 },
        };
        private readonly ICategoriesRepository _categoriesRepository;

        private ProductsRepository()
        {
            _categoriesRepository = CategoriesRepository.New();
        }

        public static IProductsRepository New()
        {
            if (Self == null) Self = new ProductsRepository();
            return Self;
        }

        public void Create(Product product)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<Product> GetAll()
        {
            return Products.Select(p =>
            {
                p.Category = _categoriesRepository.GetById(p.CategoryID);
                return p;
            }).ToList();
        }

        public Product GetById(int id)
        {
            return Products.First(p => p.ID == id);
        }

        public void Update(Product product)
        {
            throw new NotImplementedException();
        }
    }
}
