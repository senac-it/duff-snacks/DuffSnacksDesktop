﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuffSnacksDesktop.Models;

namespace DuffSnacksDesktop.Repositories
{
    internal class PacksAssocsRepository : IPacksAssocsRepository
    {
        private static IPacksAssocsRepository Self { get; set; }
        private IProductsRepository _productsRepository { get; set; }
        private IPacksRepository _packsRepository { get; set; }
        private ICollection<PackAssoc> PacksAssocs { get; set; }

        private PacksAssocsRepository()
        {
            _productsRepository = ProductsRepository.New();
            _packsRepository = PacksRepository.New();

            var packsAssocs = new List<PackAssoc>
            {
                new PackAssoc { ProductID = 2, PackID = 1 },
                new PackAssoc { ProductID = 5, PackID = 1 },
                new PackAssoc { ProductID = 7, PackID = 1 },

                new PackAssoc { ProductID = 1, PackID = 2 },
                new PackAssoc { ProductID = 6, PackID = 2 },
                new PackAssoc { ProductID = 7, PackID = 2 },

                new PackAssoc { ProductID = 6, PackID = 3 },
                new PackAssoc { ProductID = 2, PackID = 3 },
                new PackAssoc { ProductID = 1, PackID = 3 },

                new PackAssoc { ProductID = 3, PackID = 4 },
                new PackAssoc { ProductID = 7, PackID = 4 },
                new PackAssoc { ProductID = 6, PackID = 4 },
            };

            foreach (var packAssoc in packsAssocs) Create(packAssoc);
        }


        public static IPacksAssocsRepository New()
        {
            if (Self == null) Self = new PacksAssocsRepository();
            return Self;
        }

        public void Create(PackAssoc packsAssoc)
        {
            packsAssoc.ID = PacksAssocs.Count + 1;
            packsAssoc.Product = _productsRepository.GetById(packsAssoc.ProductID);
            packsAssoc.Pack = _packsRepository.GetById(packsAssoc.PackID);

            PacksAssocs.Add(packsAssoc);
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<PackAssoc> GetAll()
        {
            throw new NotImplementedException();
        }

        public PackAssoc GetById(int id)
        {
            return PacksAssocs.First(x => x.ID == id);
        }

        public void Update(PackAssoc packsAssoc)
        {
            throw new NotImplementedException();
        }
    }
}
