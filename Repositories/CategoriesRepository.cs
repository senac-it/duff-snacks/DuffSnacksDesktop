﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuffSnacksDesktop.Models;

namespace DuffSnacksDesktop.Repositories
{
    internal class CategoriesRepository : ICategoriesRepository
    {
        private static ICategoriesRepository Self { get; set; }
        private ICollection<Category> Categories = new List<Category>
        {
            new Category { ID = 1, Name = "Salgadinho", Description = "", },
            new Category { ID = 2, Name = "Refrigerantes", Description = "", },
            new Category { ID = 3, Name = "Enérgeticos", Description = "", },
            new Category { ID = 4, Name = "Doces", Description = "", },
        };

        private CategoriesRepository() { }

        public static ICategoriesRepository New()
        {
            if (Self == null) Self = new CategoriesRepository();
            return Self;
        }
        public void Create(Category category)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public ICollection<Category> GetAll()
        {
            return Categories;
        }

        public Category GetById(int id)
        {
            return Categories.First(c => c.ID == id);
        }

        public void Update(Category category)
        {
            throw new NotImplementedException();
        }
    }
}
