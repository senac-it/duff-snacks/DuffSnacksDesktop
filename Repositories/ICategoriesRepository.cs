﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DuffSnacksDesktop.Models;

namespace DuffSnacksDesktop.Repositories
{
    internal interface ICategoriesRepository
    {
        ICollection<Category> GetAll();

        Category GetById(int id);
        void Create(Category category);
        void Update(Category category);
        void Delete(int id);
    }
}
